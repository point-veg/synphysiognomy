
<!-- README.md is generated from README.Rmd. Please edit that file -->

# synphysiognomy

<!-- badges: start -->
<!-- badges: end -->

Package `synphysiognomy` collects R functions to analyse the vegetation
macrostructure of the relevés of a phytosociological table.

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/point-veg/synphysiognomy")
```
