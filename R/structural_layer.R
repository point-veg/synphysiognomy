# structural_layer.R
#'
#' @title Structural layer
#'
#' @description Identifies the structural layer for each relevé in a phytosociological table.
#'
#' @param m.percent `matrix` A phytosociological table converted to percentage cover, where rows correspond to taxa and columns correspond to relevés.
#' @param layer `factor` A `vector` giving the layer for each row of `m.percent`.
#' @param hierarchy `character` A `character vector` giving the structural order/hierarchy of the layers (the ordering from the highest/more complex to the smallest/less complex layer).
#' @param threshold `numeric` A single number or a `vector`, giving the minimum summed percentage value of a layer so it can be considered the structural layer. Defaults to 20 (the value is recycled for all layers). A `vector` of the same length of `hierarchy` can be given with different thresholds for each layer (the order should be the same as in `hierarchy`).
#' @param type.of.sum `character` If "simple" (the default), the function simply sums the percentage cover within each layer. If "independent" the function returns the sum under the independence assumption (independent overlap of covers).
#'
#' @details The `type.of.sum` defaults to "simple", which might be more appropriate when aggregating different taxa within the same layer (Tichy´ et al. 2011).
#'
#' @return A `factor` with the dominating layer in each relevé. NA is introduced if the summed cover do not reach the given `threshold` in any layer.
#'
#' @references Tichy´ L., Holt, J. and Nejezchlebová M. 2011. JUICE: program for management, analysis and classification of ecological data. 1st part. 2nd Edition of the Program Manual. Czech Republic.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tiagomonteirohenriques@@gmail.com}.
#'
#' @export
#'
structural_layer <- function(m.percent, layer, hierarchy, threshold = 20, type.of.sum = "simple") {
  if (type.of.sum == "simple") {
    mat.temp <- rowsum(m.percent, layer)	 #summed cover for each phyisionomic type
    if (sum(colSums(mat.temp >= threshold) == 0)) {message("At least one relev\u00e9 didn't reach the given threshold in any layer. NA is introduced in such cases.")} #match returns NA in these cases
    mat.temp <- mat.temp[hierarchy,,drop=FALSE]
    obtained.order <- apply(mat.temp >= threshold, 2, function (x) {match(T, x)})
    lt <- length(hierarchy)
    list.temp <- vector("list", lt)
    return(factor(sapply(obtained.order, function (x) {
      hierarchy[x]
    }), levels = rev(hierarchy), ordered=TRUE))
  } else {
    if (type.of.sum == "independent") {
      stop("\"independent\" sum is still to implement")
    } else {
      stop("type.of.sum must be \"simple\" or \"independent\"")
    }
  }
}
